#ifndef MAIN_H
#define MAIN_H
#include <SDL2/SDL.h>
#include <stdbool.h>
#include <limits.h>
#include <time.h>

typedef struct Point Point;
struct Point{
	Point* next;
	unsigned x;
	unsigned y;
	uint32_t color;
};

typedef void (*MouseFunction)(Point**, unsigned, unsigned);

int init();
int run();
int term();

int keypress(SDL_Keycode, Point**);
void move_last_point(Point**, unsigned, unsigned);
void print_closest_point(Point**, unsigned, unsigned);
uint32_t next_color();
uint32_t hsv_to_bgra32(uint8_t, uint8_t, uint8_t);
int draw_areas(Point**);
int draw_points(Point**);
int draw_point(unsigned, unsigned, uint32_t);
int print_points(Point**);
void add_point(Point**, unsigned, unsigned);
void remove_point(Point**, unsigned, unsigned);
void stop_adding_point(Point**, unsigned, unsigned);
Point* closest_point(Point**, unsigned, unsigned);
Point* last_point(Point**);


#endif
