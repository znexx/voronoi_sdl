SHELL=/bin/sh
CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -Og -g -I .
LDFLAGS=-L/usr/local/lib -I../libs/ -lSDL2 -lSDL2_image -lSDL2_ttf -lm -pedantic -Wall -Wextra
DISASM=objdump
DISASMFLAGS=-d -S -M intel
SRC=$(wildcard *.c)

$(notdir $(shell pwd)): $(SRC)
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)

disassemble:
	$(DISASM) $(DISASMFLAGS) $(notdir $(shell pwd))

clean:
	rm -rf *.o
