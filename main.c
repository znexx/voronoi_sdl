#include "main.h"

static SDL_Window* window;
static SDL_Surface* surface;
static SDL_Event* event;
static bool running = true;
unsigned WINDOW_WIDTH = 640;
unsigned WINDOW_HEIGHT = 360;
unsigned DOT_SIZE = 2;

#define DEBUG 1
#define debug_print(fmt, ...) \
            do { if (DEBUG) fprintf(stderr, fmt, __VA_ARGS__); } while (0)
#define print_param(x,y) printf(#x": "#y"\n", x)

MouseFunction mouse_left;
MouseFunction mouse_move;
MouseFunction mouse_right;

int main(void){
	int status;
	printf(" - Left click to start adding a new point. Left click again to set it\n");
	printf(" - F2 to print points\n");
	printf(" - Esc to exit\n");
	if((status = init())){
		fprintf(stderr, "Init failed!\n");
		return term();
	}

	if((status = run())){
		return term();
	}

	if((status = term())){
		return status;
	}
}

int keypress(SDL_Keycode code, Point** list){
	switch(code){
		case SDLK_ESCAPE:
			running = false;
			break;
		case SDLK_F2:
			print_points(list);
			break;
	}
	return 0;
}

void move_last_point(Point** list, unsigned mx, unsigned my){
	Point* elem;
	elem = last_point(list);
	if(elem){
		elem->x = mx;
		elem->y = my;
	}
}

void print_closest_point(Point** list, unsigned mx, unsigned my){
	Point* elem;
	elem = closest_point(list, mx, my);
	if(elem){
		debug_print("Closest: x=%d, y=%d, c=%08x\n", elem->x, elem->y, elem->color);
	}
}

uint32_t next_color(){
	static uint8_t h = 0;
	static uint8_t s = 255;
	static uint8_t v = 255;
	static double t = 0;
	static uint32_t color;
	color = hsv_to_bgra32(h,s,v);
	print_param(hsv_to_bgra32(h, s, v), %08x);
	h += 37;
	s = 192+63*sin(t/3.1415926535);
	t += 0.2;
	return color;
}

uint32_t hsv_to_bgra32(uint8_t h, uint8_t s, uint8_t v){
	static uint8_t a = 255;
	uint8_t c, x, hp, r=0, g=0, b=0;
	c = s*v/255;
	hp = h/42.6;
	x = c*(1 - fabs(fmod(h/42.6, 2) - 1.0));
	switch(hp){
		case 0:
			r = c;
			g = x;
			break;
		case 1:
			r = x;
			g = c;
			break;
		case 2:
			g = c;
			b = x;
			break;
		case 3:
			g = x;
			b = c;
			break;
		case 4:
			r = x;
			b = c;
			break;
		case 5:
			r = c;
			b = x;
			break;
	}
	r += v - c;
	g += v - c;
	b += v - c;
	return (a << 24) | (r << 16) | (g << 8) | (b);
}

int draw_areas(Point** list){
	unsigned x, y;
	Point *elem;
	for(y = 0; y < WINDOW_HEIGHT; y++){
		for(x = 0; x < WINDOW_WIDTH; x++){
			elem = closest_point(list, x,y);
			if(!elem)
				return 1;
			((uint32_t*)surface->pixels)[y*WINDOW_WIDTH + x] = elem->color;
		}
	}
	return 0;
}

int draw_points(Point** list){
	Point *elem;
	if(!list)
		return 1;
	elem = *list;
	while(elem){
		draw_point(elem->x, elem->y, 0xffffffff);
		elem = elem->next;
	}
	return 0;
}

int draw_point(unsigned mx, unsigned my, uint32_t color){
	unsigned x, xmax, y, ymax;
	ymax = my + DOT_SIZE > WINDOW_HEIGHT ? WINDOW_HEIGHT : my + DOT_SIZE;
	xmax = mx + DOT_SIZE > WINDOW_WIDTH ? WINDOW_WIDTH :   mx + DOT_SIZE;
	y = (int)(my - DOT_SIZE) < 0 ? 0 : my - DOT_SIZE;
	for(; y < ymax; y++){
		x = (int)(mx - DOT_SIZE) < 0 ? 0 : mx - DOT_SIZE;
		for(; x < xmax; x++){
			((uint32_t*)surface->pixels)[y*WINDOW_WIDTH + x] = color;
		}
	}
	return 0;
}

int print_points(Point** list){
	Point* elem;
	int n = 0;
	if(!list)
		return 1;
	elem = (*list);
	debug_print("elem->next = %p\n", (void*)elem->next);
	while(elem){
		printf("Index %d: x=%03d, y=%03d, c=%08x\n", n++, elem->x, elem->y, elem->color);
		elem = elem->next;
	}
	return 0;
}

void add_point(Point** list, unsigned mx, unsigned my){
	Point* elem = calloc(1, sizeof(Point));
	elem->x = mx;
	elem->y = my;
	elem->color = next_color();
	elem->next = NULL;
	if(!(*list)){
		(*list) = elem;
	}else{
		(last_point(list))->next = elem;
	}
	mouse_left = &stop_adding_point;
}

void remove_point(Point** list, unsigned mx, unsigned my){
	Point* elem;
	Point* last;
	Point* point;
	if(!list)
		return;
	point = closest_point(list, mx, my);
	if(!point)
		return;
	elem = (*list);
	last = elem;
	while(elem){
		if(elem == point){
			last->next = elem->next;
			free(elem);
		}
		last = elem;
		elem = elem->next;
	}
}

void stop_adding_point(Point** list, unsigned mx, unsigned my){
	debug_print("Added point at (%d:%d)\n", mx, my);
	list = list;
	mouse_left = &add_point;
}

Point* closest_point(Point** list, unsigned x, unsigned y){
	Point *elem, *closest = NULL;
	unsigned d, cd;
	long dx, dy;
	if(!list)
		return NULL;
	elem = (*list);
	cd = UINT_MAX;
	while(elem){
		dx = x - elem->x;
		dy = y - elem->y;
		d = dx*dx + dy*dy;
		//d = abs(dx) + abs(dy);
		if(d < cd){
			closest = elem;
			cd = d;
		}
		elem = elem->next;
	}
	return closest;
}

Point* last_point(Point** list){
	Point* elem;
	if(!list)
		return NULL;
	elem = (*list);
	if(!elem){
		return elem;
	}
	while(elem->next){
		elem = elem->next;
	}
	return elem;
}

int run(){
	Point** list = calloc(1, sizeof(Point*));
	while(running){
		while(SDL_PollEvent(event)){
			switch(event->type){
				case SDL_QUIT:
					running = false;
					break;
				case SDL_KEYDOWN:
					keypress(event->key.keysym.sym, list);
					break;
				case SDL_MOUSEBUTTONDOWN:
					switch(event->button.button){
						case SDL_BUTTON_LEFT:
							mouse_left(list, (unsigned)event->button.x, (unsigned)event->button.y);
							break;
						case SDL_BUTTON_RIGHT:
							mouse_right(list, (unsigned)event->button.x, (unsigned)event->button.y);
							break;
					}
					break;
				case SDL_MOUSEMOTION:
					if(mouse_left == &stop_adding_point){
						mouse_move(list, (unsigned)event->motion.x, (unsigned)event->motion.y);
					}
					break;
			}
		}

		draw_areas(list);
		draw_points(list);

		SDL_UpdateWindowSurface(window);
	}
	return 0;
}

int init(){
	SDL_Init(SDL_INIT_VIDEO);

	if(!(window = SDL_CreateWindow("Window 1",
					SDL_WINDOWPOS_UNDEFINED,
					SDL_WINDOWPOS_UNDEFINED,
					WINDOW_WIDTH,
					WINDOW_HEIGHT,
					0)))
		return fprintf(stderr, "Could not create window\n");

	if(!(surface = SDL_GetWindowSurface(window)))
		return fprintf(stderr, "Unable to get window surface\n");

	event = calloc(1, sizeof(SDL_Event));

	mouse_left = &add_point;
	mouse_move = &move_last_point;
	mouse_right = &print_closest_point;
	return 0;
}

int term(){
	free(event);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
